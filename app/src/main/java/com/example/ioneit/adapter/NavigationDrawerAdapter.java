package com.example.ioneit.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ioneit.activity.R;
import com.example.ioneit.model.NavDrawerItem;

import java.util.Collections;
import java.util.List;

/**
 * Created by Knightzoro on 12/18/15.
 */
public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {

    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    private int selectedItem = 1;

    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public NavigationDrawerAdapter(List<NavDrawerItem> data, LayoutInflater inflater, Context context) {
        this.data = data;
        this.inflater = inflater;
        this.context = context;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemChanged(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawerItem current = data.get(position);
        holder.title.setText(current.getTitle());
        holder.imageView.setImageResource(current.getIcon());

        if(selectedItem == position) {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
            holder.title.setTextColor(context.getResources().getColor(R.color.textColorPrimary));
            holder.itemView.setSelected(true);
        }
        else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
            holder.title.setTextColor(Color.GRAY);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView imageView;

        public MyViewHolder(View itemView) {
            super(itemView);

            // Handle item click and set the selection
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Redraw the old selection and the new
                    notifyItemChanged(selectedItem);
                    selectedItem = getLayoutPosition();
                    // v.setBackgroundColor(Color.CYAN);
                    // Log.d("aa", String.valueOf(selectedItem));
                    notifyItemChanged(selectedItem);
                }
            });

            title = (TextView) itemView.findViewById(R.id.title);
            imageView = (ImageView) itemView.findViewById(R.id.rowIcon);
        }
    }
}
