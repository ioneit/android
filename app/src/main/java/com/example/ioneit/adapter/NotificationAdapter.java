package com.example.ioneit.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ioneit.activity.R;
import com.example.ioneit.model.NotificationItem;
import com.example.ioneit.service.ImageLoadTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Knightzoro on 1/6/16.
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private static NotificationClickListener clickListener;
    private List<NotificationItem> mDataset = new ArrayList<>();

    /**
     * Provide a reference to the view for each data item
     * Complex data items may need more the one view per  item, and
     * you provide access to all the views for a data item in a view holder
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // each data item is just a string in this case
        public TextView txtHeader;
        public TextView txtFooter;
        public ImageView imageView;

        public ViewHolder(View v) {
            super(v);
            txtHeader = (TextView) v.findViewById(R.id.firstLine_notification);
            txtFooter = (TextView) v.findViewById(R.id.secondLine_notification);
            imageView = (ImageView) v.findViewById(R.id.icon_notification);

            Log.i(LOG_TAG, "Adding Listener");
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getPosition(), v);
        }
    }

    public void setOnItemClickListener(NotificationClickListener myClickListener) {
        this.clickListener = myClickListener;
    }

    public void add(int position, NotificationItem item) {
        mDataset.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    // Provide a suitable constructor (depends on the kind of dataSet)
    public NotificationAdapter(List<NotificationItem> dataset) {
        mDataset = dataset;
    }

    // create new views (invoked by the layout manager)
    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, parent, false);
        // set the view's size, margins, padding and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        NotificationItem item = mDataset.get(position);

        new ImageLoadTask(item.getUrlImage(), holder.imageView).execute();

        holder.txtHeader.setText(item.getUserName() + " commented your image");
//        holder.txtFooter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // remove(name);
//            }
//        });
        holder.txtFooter.setText(item.getTime());
    }

    // return the size of dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface NotificationClickListener {
        public void onItemClick(int position, View v);
    }

}

