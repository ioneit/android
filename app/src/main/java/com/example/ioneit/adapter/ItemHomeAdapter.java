package com.example.ioneit.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ioneit.activity.R;
import com.example.ioneit.model.Home;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by thanh_000 on 12/30/2015.
 */
public class ItemHomeAdapter extends
        RecyclerView.Adapter<ItemHomeAdapter.ViewHolder> {

   // private int deviceWidth = PrefUtils.getDeviceWidth();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.item_home, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // Get the data model based on position
        Home home = mHomes.get(position);

        holder.bind(home,listener);

        /*// Set item views based on the data model
        TextView textViewForBudget = holder.nameTextViewForBudget;
        textViewForBudget.setText(home.getBudget());

        TextView textViewForTitle = holder.nameTextViewForTitle;
        textViewForTitle.setText(home.getTitle());

        TextView textViewForComment = holder.nameTextViewForComment;
        textViewForComment
                .setText(home.getComment());

        TextView textViewForFollow = holder.nameTextViewForFollow;
        textViewForFollow.setText(home.getFollow());

        ImageView imageView = holder.image;
        //new ImageLoadTask("http://file.vforum.vn/hinh/2013/10/hot-girl-chi-pu-12.jpg", imageView).execute();

        Picasso.with(imageView.getContext())
                .load(home.getImage())
                .resize(350,200)
                .centerCrop()
                .into(imageView);


        Button button = holder.messageButton;

        button.setText("Help me find it");*/



    }

    @Override
    public int getItemCount() {
        return mHomes.size();
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView nameTextViewForBudget;
        public TextView nameTextViewForTitle;
        public TextView nameTextViewForComment;
        public TextView nameTextViewForFollow;
        public Button messageButton;
        public ImageView image;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            nameTextViewForBudget = (TextView) itemView.findViewById(R.id.budget_name);

            nameTextViewForTitle = (TextView) itemView.findViewById(R.id.title_name);

            nameTextViewForComment = (TextView) itemView.findViewById(R.id.count_comment);

            nameTextViewForFollow = (TextView) itemView.findViewById(R.id.count_follow);

            messageButton = (Button) itemView.findViewById(R.id.message_button);

            image = (ImageView) itemView.findViewById(R.id.item_home_image);
        }

        public void bind(final Home home, final OnItemClickListener listener){
            // Set item views based on the data model
            nameTextViewForBudget.setText(home.getBudget());

            nameTextViewForTitle.setText(home.getTitle());

            nameTextViewForComment.setText(home.getComment());

            nameTextViewForFollow.setText(home.getFollow());

            //ImageView imageView = holder.image;
            //new ImageLoadTask("http://file.vforum.vn/hinh/2013/10/hot-girl-chi-pu-12.jpg", imageView).execute();

            Picasso.with(itemView.getContext())
                    .load(home.getImage())
                    .resize(350,200)
                    .centerCrop()
                    .into(image);

            messageButton.setText("Help me find it");

            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    listener.onItemClick(home);
                }
            });
        }
    }

    // Store a member variable for the contacts
    private List<Home> mHomes;

    //refer: http://antonioleiva.com/recyclerview-listener/
    public interface OnItemClickListener {
        void onItemClick(Home item);
    }

    private final OnItemClickListener listener;

    // Pass in the contact array into the constructor
    public ItemHomeAdapter(List<Home> homes, OnItemClickListener listener) {
        mHomes = homes;
        this.listener = listener;
    }
}
