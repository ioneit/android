package com.example.ioneit.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ioneit.activity.R;
import com.example.ioneit.adapter.NotificationAdapter;
import com.example.ioneit.common.DividerItemDecoration;
import com.example.ioneit.model.NavDrawerItem;
import com.example.ioneit.model.NotificationItem;

import java.util.ArrayList;
import java.util.List;

public class NotificationFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<NotificationItem> mDataset = new ArrayList<>();

    public NotificationFragment () {

    }

    public List<NotificationItem> getData() {
        List<NotificationItem> data = new ArrayList<>();

        NotificationItem item = null;
        // preparing navigation drawer items
        for (int i = 0; i < 20; i++) {
            item = new NotificationItem("RuaCon", "2 hours ago", "http://pisces.bbystatic.com/image2/BestBuy_US/en_US/images/abn/2014/mob/pm/cat/offers-cell-phone-icon.jpg");
            data.add(item);
        }
        return data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_notification, container, false);

        mRecyclerView = (RecyclerView) layout.findViewById(R.id.recycler_view_of_notification);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new NotificationAdapter(getData());
        mRecyclerView.setAdapter(mAdapter);

        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);
        mRecyclerView.addItemDecoration(itemDecoration);

        // Inflate the layout for this fragment
        return layout;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((NotificationAdapter) mAdapter).setOnItemClickListener(new
            NotificationAdapter.NotificationClickListener() {
            @Override
                public void onItemClick(int position, View v) {
                    Log.i("Test", " Clicked on Item " + position);
                }
            });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
