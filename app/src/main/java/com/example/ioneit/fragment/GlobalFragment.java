package com.example.ioneit.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ioneit.activity.R;
import com.example.ioneit.adapter.ItemHomeAdapter;
import com.example.ioneit.model.Home;

/**
 * A simple {@link Fragment} subclass.
 */
public class GlobalFragment extends Fragment {


    public GlobalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_global, container, false);

        // Lookup the recyclerview in activity layout
        RecyclerView rvContacts = (RecyclerView) rootView.findViewById(R.id.rvGlobal);
        // Create adapter passing in the sample user data

        ItemHomeAdapter.OnItemClickListener listener = new ItemHomeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Home item) {

            }
        };

        ItemHomeAdapter adapter = new ItemHomeAdapter(
                Home.createContactsList(20, "http://media.doisongphapluat.com/297/2014/9/19/hot-boy-huynh-anh-1.PNG"), listener);
        // Attach the adapter to the recyclerview to populate items
        rvContacts.setAdapter(adapter);
        // Set layout manager to position the items
        rvContacts.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        // That's all!
        // Inflate the layout for this fragment

        return rootView;
    }

}
