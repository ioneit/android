package com.example.ioneit.fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ioneit.activity.R;
import com.example.ioneit.adapter.CategoryAdapter;
import com.example.ioneit.model.Category;

import java.util.ArrayList;


public class CategoryFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public CategoryFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_category, container, false);

        // Inflate the layout for this fragment
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.category_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        //mLayoutManager = new LinearLayoutManager(getActivity());

        int columnNumber = 2;
        mLayoutManager = new GridLayoutManager(getActivity(),columnNumber);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //TODO: change this dataSet
        //String[] myDataset = {"a","b","c","a","b","c","a","b","c","a","b","c"};
        ArrayList<Category> categories = Category.createDummyData();

        // specify an adapter (see also next example)
        mAdapter = new CategoryAdapter(categories);
        mRecyclerView.setAdapter(mAdapter);




        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
