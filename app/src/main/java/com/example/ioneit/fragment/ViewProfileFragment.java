package com.example.ioneit.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ioneit.activity.ChangePassActivity;
import com.example.ioneit.activity.LocationActivity;
import com.example.ioneit.activity.ProfileActivity;
import com.example.ioneit.activity.R;
import com.example.ioneit.activity.SettingNotificationActivity;
import com.example.ioneit.activity.SignUpActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewProfileFragment extends Fragment {


    private CardView notiCardView;
    private CardView changePassCardView;
    private CardView changeLocationCardView;

    public ViewProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_view_profile, container, false);

        //set onlick for notification
        notiCardView = (CardView) rootView.findViewById(R.id.card_view_noti);

        notiCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("++++++ Test ++++++", " Clicked on Item setting notification ");
                Intent intent = new Intent(getActivity(), SettingNotificationActivity.class);
                startActivity(intent);

            }
        });

        //set onclick for change password
        changePassCardView = (CardView) rootView.findViewById(R.id.card_view_pass);

        changePassCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("++++++ Test ++++++", " Clicked on Item change pass word ");
                Intent intent = new Intent(getActivity(), ChangePassActivity.class);
                startActivity(intent);
            }
        });

        //set onclick for change location
        changeLocationCardView = (CardView) rootView.findViewById(R.id.card_view_location);

        changeLocationCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("++++++ Test ++++++", " Clicked on Item location ");
                Intent intent = new Intent(getActivity(), LocationActivity.class);
                startActivity(intent);
            }
        });


        // Inflate the layout for this fragment
        return rootView;
    }

}
