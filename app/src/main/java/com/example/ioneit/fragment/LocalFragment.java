package com.example.ioneit.fragment;


import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ioneit.activity.ItemDetailActivity;
import com.example.ioneit.activity.R;
import com.example.ioneit.adapter.ItemHomeAdapter;
import com.example.ioneit.model.Home;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocalFragment extends Fragment {


    public LocalFragment() {
        // Required empty public constructor
    }

    /*OnItemClickLocalFragmentListener mCallback;

    public interface OnItemClickLocalFragmentListener {
        public void onItemClick(Home item);
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnItemClickLocalFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnItemClickLocalFragmentListener");
        }
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_local, container, false);

        // Lookup the recyclerview in activity layout
        RecyclerView rvContacts = (RecyclerView) rootView.findViewById(R.id.rvLocal);

        ItemHomeAdapter.OnItemClickListener listener = new ItemHomeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Home item) {
                //TODO: add switch fragment method here

                Intent i = new Intent(getActivity(), ItemDetailActivity.class);
                startActivity(i);

                //mCallback.onItemClick(item);

            }
        };

        // Create adapter passing in the sample user data
        ItemHomeAdapter adapter = new ItemHomeAdapter(
                Home.createContactsList(20, "http://file.vforum.vn/hinh/2013/10/hot-girl-chi-pu-12.jpg"), listener);
        // Attach the adapter to the recyclerview to populate items
        rvContacts.setAdapter(adapter);
        // Set layout manager to position the items
        rvContacts.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        // That's all!
        // Inflate the layout for this fragment
//
//        Toast toast = new Toast(getApplicationContext());
//        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//        toast.setDuration(Toast.LENGTH_LONG);
//        toast.setView(rootView);
//        toast.show();


        return rootView;
    }

}
