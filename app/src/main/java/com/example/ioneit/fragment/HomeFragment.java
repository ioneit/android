package com.example.ioneit.fragment;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ioneit.activity.R;
import com.example.ioneit.adapter.TabViewAdapter;
import com.example.ioneit.model.Home;


public class HomeFragment extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        //rootView.set
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
    private void setupViewPager(ViewPager viewPager) {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        // FragmentManager fragmentManager = activity.getSupportFragmentManager();
        TabViewAdapter adapter = new TabViewAdapter( activity.getSupportFragmentManager());
        adapter.addFragment(new LocalFragment(), "LOCAL");
        adapter.addFragment(new GlobalFragment(), "GLOBAL");
        viewPager.setAdapter(adapter);
    }
}