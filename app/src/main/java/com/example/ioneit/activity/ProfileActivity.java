package com.example.ioneit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.ioneit.adapter.TabViewAdapter;
import com.example.ioneit.fragment.GlobalFragment;
import com.example.ioneit.fragment.LocalFragment;
import com.example.ioneit.fragment.ViewProfileFragment;
import com.squareup.picasso.Picasso;

public class ProfileActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        toolbar = (Toolbar) findViewById(R.id.toolbarForProfile);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.containerForProfile);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabForProfile);
        tabLayout.setupWithViewPager(viewPager);

        ImageView imageViewCover = (ImageView) findViewById(R.id.profile_cover_image);
        //new ImageLoadTask("http://file.vforum.vn/hinh/2013/10/hot-girl-chi-pu-12.jpg", imageView).execute();

        Picasso.with(imageViewCover.getContext())
                .load("http://a9.vietbao.vn/images/vi955/2014/1/55604413-1389414246-hot-girl-8.jpg")
                .resize(350,200)
                .centerCrop()
                .into(imageViewCover);

        ImageView imageViewProfile = (ImageView) findViewById(R.id.circular_profile_image_view);
        //new ImageLoadTask("http://file.vforum.vn/hinh/2013/10/hot-girl-chi-pu-12.jpg", imageView).execute();

        Picasso.with(imageViewCover.getContext())
                .load("http://res.vtc.vn/media/vtcnews/2013/12/28/hot-girl-anh-the-0_1.jpg")
                .resize(80,80)
                .centerCrop()
                .into(imageViewProfile);


        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("Profile");
        collapsingToolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        //collapsingToolbar.setTitleEnabled(true);


        // back to Home when click <-
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Log.i("ProfileActivity", "main Activity activated.");

        Intent myIntent = new Intent(ProfileActivity.this, MainActivity.class);
        startActivity(myIntent);

        finish();
    }

    private void setupViewPager(ViewPager viewPager) {
        TabViewAdapter adapter = new TabViewAdapter(getSupportFragmentManager());
        adapter.addFragment(new LocalFragment(), "LOCAL");
        adapter.addFragment(new GlobalFragment(), "GLOBAL");
        adapter.addFragment(new ViewProfileFragment(), "PROFILE");
        viewPager.setAdapter(adapter);
    }


}
