package com.example.ioneit.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.ioneit.fragment.CategoryFragment;
import com.example.ioneit.fragment.ContactUsFragment;
import com.example.ioneit.fragment.FragmentDrawer;
import com.example.ioneit.fragment.HomeFragment;
import com.example.ioneit.fragment.NotificationFragment;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener{

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);



        // display the first navigation drawer view on app launch
        displayView(1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id == R.id.action_search){
            Toast.makeText(getApplicationContext(), "Search action is selected!", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        Activity activity = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                fragment = new NotificationFragment();
                title = getString(R.string.title_notification);
                break;
            case 1:
                fragment = new HomeFragment();
                title = getString(R.string.title_home);
                break;
            case 2:
                fragment = new CategoryFragment();
                title = getString(R.string.title_categories);
                break;
            case 4:
                //fragment = new ContactUsFragment();
                //title = getString(R.string.title_contactUs);
                Log.i("MainActivity", "Profile Activity activated.");

                Intent myIntent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(myIntent);

                finish();
                break;
            case 6:
                fragment = new ContactUsFragment();
                title = getString(R.string.title_contactUs);
                break;
            default:
                break;
        }



        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title

            getSupportActionBar().setTitle(title);
        }
    }

}
