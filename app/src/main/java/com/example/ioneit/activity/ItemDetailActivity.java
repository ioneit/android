package com.example.ioneit.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.example.ioneit.activity.R;
import com.squareup.picasso.Picasso;

public class ItemDetailActivity extends AppCompatActivity {

    ImageView userAvatar;
    SliderLayout sliderShow;
    ImageView commentUserAvatar;
    ImageView commentUserAvatar2;
    ImageView commentUserAvatar3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarItemDetail);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Game of Thrones Collection");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        userAvatar = (ImageView) findViewById(R.id.circular_profile_image_view);
        Picasso.with(userAvatar.getContext())
                .load("http://res.vtc.vn/media/vtcnews/2013/12/28/hot-girl-anh-the-0_1.jpg")
                .resize(30,30)
                .centerCrop()
                .into(userAvatar);

        sliderShow = (SliderLayout) findViewById(R.id.slider);

        DefaultSliderView textSliderView = new DefaultSliderView(this);
        textSliderView
                .description("Game of Thrones")
                .image("http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");

        sliderShow.addSlider(textSliderView);
        sliderShow.addSlider(textSliderView);

        commentUserAvatar = (ImageView) findViewById(R.id.comment_user_avatar);
        Picasso.with(commentUserAvatar.getContext())
                .load("http://res.vtc.vn/media/vtcnews/2013/12/28/hot-girl-anh-the-0_1.jpg")
                .resize(30,30)
                .centerCrop()
                .into(commentUserAvatar);

        commentUserAvatar2 = (ImageView) findViewById(R.id.comment_user_avatar2);
        Picasso.with(commentUserAvatar2.getContext())
                .load("http://res.vtc.vn/media/vtcnews/2013/12/28/hot-girl-anh-the-0_1.jpg")
                .resize(30,30)
                .centerCrop()
                .into(commentUserAvatar2);

        commentUserAvatar3 = (ImageView) findViewById(R.id.comment_user_avatar3);
        Picasso.with(commentUserAvatar3.getContext())
                .load("http://res.vtc.vn/media/vtcnews/2013/12/28/hot-girl-anh-the-0_1.jpg")
                .resize(30,30)
                .centerCrop()
                .into(commentUserAvatar3);




    }

    @Override
    protected void onStop() {
        sliderShow.stopAutoCycle();
        super.onStop();
    }


}
