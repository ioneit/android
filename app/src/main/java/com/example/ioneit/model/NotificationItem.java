package com.example.ioneit.model;

/**
 * Created by Knightzoro on 1/7/16.
 */
public class NotificationItem {
    private String userName;
    private String time;
    private String urlImage;

    public NotificationItem() {

    }

    public NotificationItem(String userName, String time, String urlImage) {
        this.userName = userName;
        this.time = time;
        this.urlImage = urlImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
}
