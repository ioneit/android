package com.example.ioneit.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thanh_000 on 12/29/2015.
 */
public class Home {
    private String mBudget;
    private String mImage;
    private String mTitle;
    private String mComment;
    private String mFollow;
    private boolean mOnline;

    public Home(String budget, String image, String title, String comment, String follow, boolean online) {
        mBudget = budget;
        mImage = image;
        mTitle = title;
        mComment = comment;
        mFollow = follow;

        mOnline = online;
    }

    public String getBudget() {
        return mBudget;
    }

    public String getImage() {
        return mImage;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getComment() {
        return mComment;
    }

    public String getFollow() {
        return mFollow;
    }

    public boolean isOnline() {
        return mOnline;
    }

    private static int lastContactId;

    public static List<Home> createContactsList(int numContacts, String image) {
        List<Home> homes = new ArrayList<Home>();
        lastContactId = 100;
        for (int i = 1; i <= numContacts; i++) {
            homes.add(new Home("Budget: " + ++lastContactId + " $", image, "What is picture title", "105", "5", i <= numContacts / 2));
        }

        return homes;
    }
}
