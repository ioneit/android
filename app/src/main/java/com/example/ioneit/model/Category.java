package com.example.ioneit.model;

import java.util.ArrayList;

/**
 * Created by nguyenphusi on 12/30/15.
 */
public class Category {
    private String name;
    private String imageUrl;

    public Category(String name, String imageUrl) {
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public static ArrayList<Category> createDummyData(){
        ArrayList<Category> categories = new ArrayList<Category>();
        String imageUrl = "http://pisces.bbystatic.com/image2/BestBuy_US/en_US/images/abn/2014/mob/pm/cat/offers-cell-phone-icon.jpg";
        Category category = new Category("Electronic","https://cdn2.iconfinder.com/data/icons/social-buttons-2/512/tv-256.png");
        categories.add(category);
        category = new Category("Car","http://icons.iconarchive.com/icons/graphicloads/100-flat-2/256/car-icon.png");
        categories.add(category);
        category = new Category("Mobile","https://cdn4.iconfinder.com/data/icons/technology-devices-1/500/smartphone-256.png");
        categories.add(category);
        category = new Category("Clothes","https://cdn4.iconfinder.com/data/icons/green-shopper/1054/clothes.png");
        categories.add(category);
        category = new Category("Toy","https://cdn3.iconfinder.com/data/icons/luchesa-vol-9/128/Toy-256.png");
        categories.add(category);
        category = new Category("Shoes","http://plainicon.com/dboard/userprod/2831_9307e/prod_thumb/plainicon.com-53665-w-512px-bf02.png");
        categories.add(category);

        return categories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}

